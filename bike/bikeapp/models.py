from __future__ import unicode_literals

from django.db import models

import django_filters

class Trip(models.Model):
	id = models.IntegerField(primary_key=True)
	date = models.CharField(max_length=50)
	duration = models.IntegerField(max_length=50)
	count = models.IntegerField(max_length=50)
	distance = models.IntegerField(max_length=50)
	
	class Meta:
		managed = False
		db_table = 'daily'

class DateFilter(django_filters.FilterSet):
    date = django_filters.CharFilter(lookup_expr='iexact')

	# distance = django_filters.NumberFilter()
	# distance__gt = django_filters.NumberFilter(field_name='distance', lookup_expr='gt')
    # distance__lt = django_filters.NumberFilter(field_name='distance', lookup_expr='lt')


    class Meta:
        model = Trip
        fields = {'date':['lt','gt'], 'duration':['lt','gt']}