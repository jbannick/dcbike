from __future__ import unicode_literals
from django.core.paginator import Paginator
from django.shortcuts import render

from django.shortcuts import render, redirect, get_object_or_404
# from .forms import TripForm
from .models import Trip,DateFilter
from django.views.generic import ListView, DetailView

import folium

#home view for posts. Posts are displayed in a list
class IndexView(ListView):
	template_name='index.html'
	context_object_name = 'post_list'
	def get_queryset(self):
		return Trip.objects.all()
def detailview(request,pk,template_name='post-detail.html'):
	trip= get_object_or_404(Trip, pk=pk)
	return render(request,template_name,{'object':trip})

def listing(request):
    contact_list = Trip.objects.all()
    paginator = Paginator(contact_list, 25) # Show 25 contacts per page

    page = request.GET.get('page')
    contacts = paginator.get_page(page)
    return render(request, 'list.html', {'contacts': contacts})

def product_list(request):
    f = DateFilter(request.GET, queryset=Trip.objects.all())
    return render(request, 'lists.html', {'filter': f})
